package main

import "flag"

var serverPort int

func init() {
	flag.IntVar(&serverPort, "p", 8080, "设置ws服务器端口")
}

func main() {
	flag.Parse() // 命令行解析
	server := NewServer(serverPort)
	server.Start()
}
