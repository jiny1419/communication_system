/*
本系统的json分为两大类
1. client发送到server上的，定义为：sendMsg
2. server发送给client的，定义为：replyMsg

sendMsg的Type：
	- "login"			表示该json为__请求登录__数据包，data包含"account" "password"
	- "others"			表示该json为__群聊消息__数据包，data包含"content"
	- "private"			表示该json为__私聊消息__数据包，data包含"toperson" "content"
	- "instruction"		暂未实际使用

replyMsg的Type：
	- "hint"		表示该json为__提示消息__数据包，data包含"content"
	- "msg"			表示该json为__聊天消息__数据包，data包含"fromperson" "content"
	- "sys"			表示该json为__后台消息__数据包，data包含"tag" "data"

*/

package main

type Json struct {
	Type string            `json:"type"`
	Data map[string]string `json:"data"`
}
