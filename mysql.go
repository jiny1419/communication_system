package main

import (
	"fmt"

	_ "github.com/go-sql-driver/mysql"
	"github.com/jmoiron/sqlx"
)

type MyDB struct {
	FD *sqlx.DB
}

type loginInfo struct {
	Account string `db:"account"`
	Pwd     string `db:"password"`
	Name    string `db:"nickname"`
}

func NewDB(dbconf DBConfig) *MyDB {
	DB, err := sqlx.Open(dbconf.Type, fmt.Sprintf("%s:%s@tcp(%s:%d)/%s", dbconf.User, dbconf.Password, dbconf.Ip, dbconf.Port, dbconf.DBName))
	if err != nil {
		fmt.Println("open mysql failed,", err)
		return nil
	}
	DB.SetMaxOpenConns(200)
	DB.SetMaxIdleConns(10)
	myDB := &MyDB{
		FD: DB,
	}
	return myDB
}

func (myDB *MyDB) Login(account string, pwd string) (string, bool) {
	// fmt.Println("========== in DB ==========")D
	sqlStr := "select account, password, nickname from user where account = ?;"
	var info loginInfo
	// err := myDB.FD.Get(&info, sqlStr, account)
	err := myDB.FD.Get(&info, sqlStr, account)
	if err != nil {
		// 打印错误
		return "[ system ]  查询错误：" + fmt.Sprintf("%v", err), false
	}
	// fmt.Println(account, pwd)
	// fmt.Println(info)
	if info.Pwd == pwd {
		return info.Name, true
	} else if info.Account == account {
		return "[ system ]  密码错误", false
	} else {
		return "[ system ]  账号不存在", false
	}
}
