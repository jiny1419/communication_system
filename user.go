package main

import (
	"log"

	"github.com/gorilla/websocket"
)

type User struct {
	LogTag      bool
	Account     string
	Name        string
	Addr        string
	SelfMessage chan Json
	conn        *websocket.Conn
	server      *Server
}

// 创建一个用户的API
func NewUser(conn *websocket.Conn, server *Server) *User {
	userAddr := conn.RemoteAddr().String()
	// 连接数据库根据account获取nickname->Name

	user := &User{
		LogTag:      false,
		Account:     userAddr,
		Name:        userAddr,
		Addr:        userAddr,
		SelfMessage: make(chan Json),
		conn:        conn,
		server:      server,
	}
	// 启动监听user channel消息的goroutine
	go user.ListenMessage()
	return user
}

// 处理用户上传请求
func (user *User) SendMsgHandler(sendMsg Json) {
	switch sendMsg.Type {
	case "login":
		// 登录json
		user.Login(sendMsg)
	case "others":
		// 发送给其他人
		user.BroadCastMessage(sendMsg)
	case "private":
		// 私聊
		user.ToMessage(sendMsg)
	case "instruction":
		// 指令
	default:
	}
}

// 用户登录
func (user *User) Login(sendMsg Json) {
	if user.LogTag {
		replyJson := Json{
			Type: "hint",
			Data: map[string]string{
				"content": "[ system ]  当前页面已登录",
			},
		}
		user.SelfMessage <- replyJson
		return
	}
	account := sendMsg.Data["account"]
	pwd := sendMsg.Data["password"]
	result, ok := user.server.Database.Login(account, pwd)
	if !ok {
		// 返回给用户result
		res := Json{
			Type: "hint",
			Data: map[string]string{
				"content": result,
			},
		}
		user.SelfMessage <- res
	} else if _, exist := user.server.OnlineMap[account]; exist {
		// 将result设置为user的name
		replyJson := Json{
			Type: "hint",
			Data: map[string]string{
				"content": "[ system ]  该账号已登录",
			},
		}
		user.SelfMessage <- replyJson
	} else {
		user.Name = result
		user.Account = account
		user.Online()
	}
}

// 用户上线
func (user *User) Online() {
	user.server.mapLock.Lock()
	user.server.OnlineMap[user.Account] = user
	user.server.mapLock.Unlock()
	user.LogTag = true
	// 广播上线消息
	json := Json{
		Type: "hint",
		Data: map[string]string{
			"content": "[ " + user.Name + " ]" + "  已上线",
		},
	}
	user.server.AllMessage <- json
	// 发送用户名称
	name := Json{
		Type: "sys",
		Data: map[string]string{
			"tag":  "username",
			"data": user.Name,
		},
	}
	user.SelfMessage <- name
}

// 用户离线
func (user *User) Offline() {
	user.server.mapLock.Lock()
	logTag := user.server.OnlineMap[user.Account].LogTag
	delete(user.server.OnlineMap, user.Account)
	user.server.mapLock.Unlock()
	// 广播下线消息
	if logTag {
		json := Json{
			Type: "hint",
			Data: map[string]string{
				"content": "[ " + user.Name + " ]" + "  已下线",
			},
		}
		user.server.AllMessage <- json
	}
}

// 处理私聊信息
func (user *User) ToMessage(sendMsg Json) {
	user.server.mapLock.Lock()
	toUser, ok := user.server.OnlineMap[sendMsg.Data["toperson"]]
	user.server.mapLock.Unlock()
	if !ok {
		replyJson := Json{
			Type: "hint",
			Data: map[string]string{
				"content": "发送对象未上线, 消息发送失败",
			},
		}
		user.SelfMessage <- replyJson
	} else {
		replyJson2 := Json{
			Type: "msg",
			Data: map[string]string{
				"fromperson": "[ 私聊 ] " + user.Name,
				"content":    "P|" + sendMsg.Data["content"],
			},
		}
		if toUser != user {
			toUser.SelfMessage <- replyJson2
		}
	}
}

// 处理群发信息
func (user *User) BroadCastMessage(sendMsg Json) {
	replyJson := Json{
		Type: "msg",
		Data: map[string]string{
			"fromperson": user.Name,
			"content":    "B|" + sendMsg.Data["content"],
		},
	}
	user.server.mapLock.Lock()
	for _, cli := range user.server.OnlineMap {
		if cli != user {
			cli.SelfMessage <- replyJson
		}
	}
	user.server.mapLock.Unlock()
}

// 监听User的消息队列
func (user *User) ListenMessage() {
	for {
		msg := <-user.SelfMessage
		err := user.conn.WriteJSON(msg)
		if err != nil {
			log.Println(err)
			// return
		}
	}
}
