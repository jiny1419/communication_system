package main

import (
	"fmt"
	"log"
	"net/http"
	"sync"

	"github.com/gorilla/websocket"
)

var upgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
	CheckOrigin: func(r *http.Request) bool {
		return true
	},
}

type Server struct {
	Port       int              // 绑定端口
	OnlineMap  map[string]*User // 记录当前在线用户
	mapLock    sync.RWMutex     // OnlineMap的互斥锁
	AllMessage chan Json        // 广播消息的入口
	Database   *MyDB            // 启动mysql资源
}

// 创建一个server的API
func NewServer(port int) *Server {
	server := &Server{
		Port:       port,
		OnlineMap:  make(map[string]*User),
		mapLock:    sync.RWMutex{},
		AllMessage: make(chan Json),
		Database:   NewDB(ReadDBConfig()),
	}
	return server
}

// 启动服务器
func (server *Server) Start() {
	// 启动监听Message的goroutine
	go server.ListenAllMessage()

	// 启动ws服务器
	http.HandleFunc("/ws", server.HandleWebSocket)
	fmt.Println("Server started on :", server.Port)
	fmt.Println(http.ListenAndServe(fmt.Sprintf(":%d", server.Port), nil))

	//阻塞
	select {}
}

// http建立连接后处理函数
func (server *Server) HandleWebSocket(w http.ResponseWriter, r *http.Request) {
	// 已创建的http连接升级为WebSocket -> conn
	conn, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		fmt.Println("test", err)
	}
	// 连接关闭后，释放conn
	defer conn.Close()

	// 创建 user
	user := NewUser(conn, server)

	for {
		// ws读取消息 -> server处理
		var sendMsg Json
		err := user.conn.ReadJSON(&sendMsg)
		if err != nil {
			log.Println("receive err: ", err)
			break
		}
		// fmt.Println(sendMsg)
		user.SendMsgHandler(sendMsg)
	}
	// user 下线
	user.Offline()
}

// 监听Message广播消息channel，有消息就发送给全部在线User
func (server *Server) ListenAllMessage() {
	for {
		msg := <-server.AllMessage
		// msg全部发送给在线User
		server.mapLock.Lock()
		for _, cli := range server.OnlineMap {
			cli.SelfMessage <- msg
		}
		server.mapLock.Unlock()
	}
}
