# communication_system

#### 介绍
一个即时聊天系统的ws服务端
> Golang学习项目, from Aceld

#### 软件架构
`main.go`: 程序入口

`server.go`: 服务类型，用于建立socket连接与处理client的信息

`user.go`: 用户类型，由于创建并记录连接，并通过类的方法处理各类信息

`mysql.go`: 建立数据库连接并提供各种数据库操作的方法

`msg.go`: 定义数据类型的结构

`config.go`: 读取配置文件内的内容并返回相应的结构信息

#### 使用说明

windows平台为例
```bash
# 直接运行
go run main.go server.go user.go mysql.go msg.go config.go

# 编译后运行
go build -o main.exe main.go server.go user.go mysql.go msg.go config.go
./main.exe

# 将文件夹chat部署到服务器上，或者在本地打开chat/index.html
```

#### 聊天室预览
![聊天室界面](./page.jpg)

#### 贡献
本项目的前端项目基于https://github.com/zfowed/charooms-html

本项目为Golang学习项目, from Aceld(https://space.bilibili.com/373073810)