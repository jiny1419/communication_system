package main

import (
	"bufio"
	"log"
	"os"
	"reflect"
	"strconv"
	"strings"
)

type DBConfig struct {
	Type     string
	User     string
	Password string
	Ip       string
	Port     int
	DBName   string
}

func ReadDBConfig() DBConfig {
	conf := &DBConfig{}
	//创建一个结构体变量的反射
	cr := reflect.ValueOf(conf).Elem()

	file, err := os.Open("./config.yaml")
	if err != nil {
		log.Fatal(err)
	}
	defer func() {
		if err = file.Close(); err != nil {
			log.Fatal(err)
		}
	}()

	stream := bufio.NewScanner(file)
	for stream.Scan() {
		//以=分割,前面为key,后面为value
		var str = stream.Text()
		if str[0] == '#' {
			continue
		}
		var index = strings.Index(str, "=")
		var key = str[0:index]
		var value = str[index+1:]
		//因为Port是int,所以我们这里要将截取的string强转成int
		if strings.Contains(key, "Port") {
			var i, err = strconv.Atoi(value)
			if err != nil {
				panic(err)
			}
			//通过反射将字段设置进去
			cr.FieldByName(key).Set(reflect.ValueOf(i))
		} else {
			//通过反射将字段设置进去
			cr.FieldByName(key).Set(reflect.ValueOf(value))
		}
	}
	err = stream.Err()
	if err != nil {
		log.Fatal(err)
	}
	//返回Config结构体变量
	return *conf
}
